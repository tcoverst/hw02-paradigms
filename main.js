// SUBMIT BUTTON

var currentSentence = "";
var currentWord = "";

var submited = document.getElementById('button-Submit'); // link to submit button
console.log('hi');

submited.onmouseup = getFormInfo;



function getFormInfo() { //gives the form info to the first API
  var text1 = document.getElementById('input-text').value;
  makeNetworkCallToApi1(text1);
}

// Calls on Api for example words
function makeNetworkCallToApi1(text1) {
  console.log(text1);
  currentWord = text1;

  var url = "https://api.dictionaryapi.dev/api/v2/entries/en/" + text1; // API 1 URL

  var xhr = new XMLHttpRequest(); //sends info to API 1
  xhr.open("GET", url, true);

  xhr.onload = function(e) { //interrupt when recieves data back from API 1
    var response = xhr.responseText;
    console.log('response is ' + response);
    API1response(text1, response);
  }

  xhr.onerror = function(e) {
    console.error('error message is ' + xhr.statusText);
  }

  xhr.send(null);
}


//Parses JSON to find example sentence with word searched
function API1response(text1, response) {
  var response_json = JSON.parse(response);
  if (response_json['title'] = "No Definitions Found") {
    var completeSentence = "No Definitions Found";
  }
  for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 10; j++) {
      try {
        var exampleSentence = response_json[0]['meanings'][j]['definitions'][i]['example'];
        if (exampleSentence != undefined && (exampleSentence.slice(-1) == '.')) {
          var completeSentence = exampleSentence;
          break;
        }
      } catch (err) {

      }
    }
  }
  console.log(completeSentence);
  makeNetworkCallToApi2(completeSentence);
}

//Calls API for images

function makeNetworkCallToApi2(text2) {
  var label1 = document.getElementById("label1");
  label1.innerHTML = text2;
  currentSentence = text2;
  if (text2 != "No Definitions Found") {

    var text2 = text2.replaceAll(' ', '%20');
    console.log(text2);
    const data = null;

    const xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function() {
      if (this.readyState === this.DONE) {
        console.log(this.responseText);
        updateUIWithPhotos(this.responseText);
      }
    });

    xhr.open("GET", "https://google-image-search1.p.rapidapi.com/v2/?q="+text2+"&hl=en");
    xhr.setRequestHeader("X-RapidAPI-Key", "678e26aee4msh91193935b599bfcp1b2695jsn0a74bd2a7d44");
    xhr.setRequestHeader("X-RapidAPI-Host", "google-image-search1.p.rapidapi.com");

    xhr.send(data);

  }
}

// Dynamically adds photos to DOM
function updateUIWithPhotos(image_url) {
  console.log(image_url);
  var outPutedJsonPics = String(image_url);
  let position = outPutedJsonPics.search("url");
  var linkUrl = "";
  for (var i = position + 6; i < outPutedJsonPics.length; i++) {

    if (outPutedJsonPics[i] == '"') {
      console.log(linkUrl);
      break;
    } else {
      linkUrl = linkUrl + outPutedJsonPics[i];
    }

  }
  console.log(linkUrl.substring(0,2))
if (linkUrl.substring(0,2) == "ht"){

  var image_fig = document.createElement("figure");
  image_fig.id = linkUrl;
  image_fig.style = "width:300px object-fit:contain padding:50px";
  document.getElementById("img-con").appendChild(image_fig);
  var image_img = document.createElement("img");
  image_img.id = linkUrl + "image";
  image_img.src = linkUrl;
  document.getElementById(linkUrl).appendChild(image_img);
  var image_sent = document.createElement("figcaption");
  image_sent.id = linkUrl + "sentence";
  image_sent.innerHTML = currentWord + ": " + currentSentence;
  document.getElementById(linkUrl).appendChild(image_sent);
}
else{
  var label1 = document.getElementById("label1");
  label1.innerHTML = "No Image Found";
}
}
